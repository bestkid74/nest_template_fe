import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpParams, HttpHeaders, HttpResponse } from '@angular/common/http';

import { of as observableOf, Observable, Subject} from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { ApiResponse, ErrorsResponse, ContentResponse } from '@app/models/response';
import { Params } from '@app/params';
import { LocalStorageHelper } from '@app/helpers/localStorage.helper';

interface Options {
  headers?: HttpHeaders;
  observe?: 'body';
  params?: HttpParams;
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}

@Injectable()
export class HttpService extends HttpClient {
  url: string;
  private entity: any;
  headers: HttpHeaders;
  private defaultHeaders: HttpHeaders;
  fields: Array<string> = [];
  inProgressObserv: Subject<boolean> = new Subject();
  private param: Params = new Params;

  constructor(
    handler: HttpHandler,
  ) {super(handler)}

  setHeaders(sendFile: boolean = false): void {
    if (this.headers) {
      this.defaultHeaders = this.headers;
    } else {
      let headerJson;
      if (sendFile) {
        headerJson = {
          'Accept': 'application/json',
          'Content-Language': 'en'
        };
      } else {
        headerJson = {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Content-Language': 'en'
        };
      }

      if (this.getAccessToken()) {
        headerJson = Object.assign(headerJson, {'Authorization': 'Bearer ' + this.getAccessToken()});
      }
      this.defaultHeaders = new HttpHeaders(headerJson);
    }
  }

  setUrl(url: string, sendFile: boolean = false): void {
    this.setHeaders(sendFile);
    this.url = this.param.apiUrl + url;
    this.addParameters();
  }

  private addParameters(): void {
    let qs = '';
    for (const key in this.fields) {
      const value = this.fields[key];
      qs += `${encodeURIComponent(key)}=${encodeURIComponent(value)}&`;
    }
    if (qs.length > 0) {
      qs = qs.substring(0, qs.length - 1); // rm last '&'
      this.url += `?${qs}`;
    }
    this.fields = [];
  }

  setEntity(entity: any): void {
    this.entity = entity;
  }

  setSendFile(): void {
    this.defaultHeaders.delete('Content-Type');
    this.defaultHeaders.append('Content-Type',  'multipart/form-data');
  }

  _get(): Promise<any> {
    return this.get(this.url, this.getOptions([], null, 'response'))
      .toPromise()
      .then(response => this.handleResponse(response))
      .catch(error => this.handleError(error));
  }

  _getObserv(): Observable<any> {
    return this.get(this.url, this.getOptions([], null, 'response')).pipe(
      map(x => this.handleResponseObserv(x)));
  }

  _post(): Promise<any> {
    return this.post(this.url, this.entity, this.getOptions())
      .toPromise()
      .then(response => this.handleResponse(response))
      .catch(error => this.handleError(error));
  }

  _put(): Promise<any> {
    return this.put(this.url, this.entity, this.getOptions())
      .toPromise()
      .then(response => this.handleResponse(response))
      .catch(error => this.handleError(error));
  }

  _delete(): Promise<void> {
    return this.delete(this.url, this.getOptions())
      .toPromise()
      .then(response => this.handleResponse(response))
      .catch(error => this.handleError(error));
  }

  protected handleResponseObserv(response: Object | HttpResponse<any>) {
    let responseBody;
    if (response instanceof HttpResponse) {
      responseBody = <ApiResponse>response.body;
    } else {
      responseBody = <ApiResponse>response;
    }
    return responseBody;
  }

  protected handleResponse(response: Object | HttpResponse<any>) {
    let responseBody;
    if (response instanceof HttpResponse) {
      responseBody = <ApiResponse>response.body;
    } else {
      responseBody = <ApiResponse>response;
    }

    if (responseBody.success) {
      return responseBody.content;
    } else {
      throw responseBody.errors;
    }
  }

  protected handleError(error: ErrorsResponse | any): Promise<any> {
    console.error(error);
    return Promise.reject(error);
  }

  private getAccessToken(): string {
    let authData: any;
    authData = LocalStorageHelper.getlocalStorage('authData');
    if (authData) {
      authData = JSON.parse(authData);
      if (authData.access_token) {
        return authData.access_token;
      }
    }
    return '';
  }

  public parseRespone(respone): ErrorsResponse | ContentResponse {
    const res: ApiResponse = respone.json();
    if (res.success) {
      return res.content;
    } else {
      return res.errors;
    }
  }

  public __get(params: any = []): Observable<any> {
    return this.get(this.url, this.getOptions(params)).pipe(
      map(response =>
        this.handleResponse(response)),
      catchError(err =>
        this.handleError(err)),);
  }

  private getOptions(params: any = [], responseType = null, observe = null) {
    let options: Options = {};
    options.headers = this.defaultHeaders;

    if (params) {
      let paramsHttp: HttpParams = new HttpParams();
      for (const k in params) {
        paramsHttp = paramsHttp.set(k, params[k].toString());
      }
      options.params = paramsHttp;
    }
    if (observe) {
      options.observe = observe;
    }
    if (responseType) {
      options.responseType = responseType;
    }
    return options;
  }

  getInProgressObserv(): Observable<any> {
    return this.inProgressObserv.asObservable();
  }

  getBlob(params: any = []): Observable<any> {
    return this.get(this.url, this.getOptions(params, 'blob')).pipe(
      catchError(this.handleObservableError())
    );
  }

  private handleObservableError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return observableOf(result as T);
    };
  }
}
