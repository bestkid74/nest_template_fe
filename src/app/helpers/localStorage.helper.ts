import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageHelper {

  static stored_time : number = 60 * 60 * 24;
  private static timePrefix:string = '_time';

  static setStorageForATime(name, value): boolean {
    const date = new Date();
    const schedule = Math.round((date.setSeconds(date.getSeconds() + this.stored_time)) / 1000);
    try {
      localStorage.setItem(name, value);
      localStorage.setItem(name + this.timePrefix, String(schedule));
    }
    catch (e) {
      return false;
    }
    return true
  }

  static setStorage(name: string, value: string): boolean {
    localStorage.setItem(name, value);
    return true
  }

  static removeStorage(name): boolean {
    try {
      localStorage.removeItem(name);
      localStorage.removeItem(name + this.timePrefix);
    } catch (e) {
      return false;
    }
    return true;
  }

  static updateLeaveStorageTime(name): void {
    const date = new Date();
    const schedule = Math.round((date.setSeconds(date.getSeconds() + this.stored_time)) / 1000);
    localStorage.setItem(name + this.timePrefix, String(schedule));
  }

  static getlocalStorage(name: string): string {
    const date = new Date();
    const current = Math.round( +date / 1000);

    let end_stored_time = localStorage.getItem(name + this.timePrefix);
    if (!end_stored_time || end_stored_time === 'null') {
      end_stored_time = '0';
    }
    if (Number(end_stored_time) < current) {
      LocalStorageHelper.removeStorage(name);
      return null;
    } else {
      LocalStorageHelper.updateLeaveStorageTime(name);
      try {
        return localStorage.getItem(name);
      } catch(e) {
        return null;
      }
    }
  }

  static getStorageValue(name): string {
    return localStorage.getItem(name);
  }
}
