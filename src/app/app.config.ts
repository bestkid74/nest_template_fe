import { InjectionToken } from '@angular/core';

export let APP_CONFIG = new InjectionToken('app.config');

const enum AuthFormTypes {
  signUp = 'sign-up',
  logIn = 'log-in'
}

export interface IAppConfig {
  authFormTypes: {
    signUp: string;
    logIn: string;
  };
}

export const AppConfig: IAppConfig = {
  authFormTypes: {signUp: AuthFormTypes.signUp, logIn: AuthFormTypes.logIn}
}
