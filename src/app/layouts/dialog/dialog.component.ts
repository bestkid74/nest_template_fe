import {Component, Inject, OnInit} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AppConfig, IAppConfig } from '../../app.config';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  readonly  appConfig: IAppConfig = AppConfig;
  authForm: FormGroup;
  hidePass = true;

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {type: string, name?: string},
    private _fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.data.type === this.appConfig.authFormTypes.signUp ? this._initSignUpForm() : this._initLogInForm();
  }

  private _initLogInForm(): void {
    this.authForm = this._fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: [null, [Validators.required, Validators.min(6)]]
    });
  }

  private _initSignUpForm(): void {
    this.authForm = this._fb.group({
      email: ['', [Validators.email, Validators.required]],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: [null, Validators.required],
      country: [null],
      city: [null],
      addressLine1: [null],
      addressLine2: [null],
      profession: [null],
      phone: [''],
      password: [null, [Validators.required, Validators.min(6)]]
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
