export interface MetaRespone {
  totalCount: number
  pageCount: number;
  currentPage: number;
  perPage: number;
}

export interface SimpleResponse {
  [key: string]: number | string | boolean;
}

export type ContentResponse = any | SimpleResponse | RegisterSellerRespone | StatusRespone;

export interface ErrorsResponse{
  code: string;
  message: string;
  details: string[] | string;
}

export interface ApiResponse {
  success: boolean;
  content?: ContentResponse;
  errors?: ErrorsResponse;
  body?: ContentResponse;
}

export interface LoginRespone {
  id: number;
  access_token: string;
}

export interface CompareTokenRespone {
  valid_token: boolean
}

export interface RecoverPasswordRespone {
  status: string;
}

export interface StatusRespone {
  status: string;
}

export interface RegisterSellerRespone {
  code_register: string;
  company_name: string;
  company_name_translit: string;
  fio: string;
  email: string;
  phone: string;
  skype: string;
}
